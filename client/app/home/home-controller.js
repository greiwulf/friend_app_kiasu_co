(function (){
  angular
    .module("friendApp")
    .controller("HomeCtrl", HomeCtrl);

    //DI services
    HomeCtrl.$inject = ['$state','FetchFriends', 'PostFeeds'];

    function HomeCtrl($state, FetchFriends, PostFeeds){
      var vm = this;
      // exposed models
      vm.myStatus = "Future's soo bright, gotta wear shades!";
      vm.updateMystatus = "";
      vm.friends = friends;
      vm.friends2 = {};

      vm.mypost = {
          from_username: "me"
          , to_username: "all"
          , msg: ""
  //        , create_date: ""
      };



      function initFriends() {
        FetchFriends
          .fetchFriends()
          .then(function(results){
            console.log("results: " + JSON.stringify(results.data));
            vm.friends2 = results.data;
          })
          .catch(function(err){
            console.info("error " + JSON.stringify(err));
          });
      }
      initFriends();

      // random minutes generator
      randTstamp = function(){
        var initTstamp = new Date();
        var newTstamp = new Date(initTstamp.getTime() + Math.floor((Math.random() * 10000) - 1000));
        return(newTstamp);
      }

      // update my status
      vm.updateStatus = function(){
        // vm.updateMystatus = vm.myStatus;
        vm.myStatus = vm.updateMystatus;
        vm.updateMystatus = "";
        vm.mypost.msg = vm.myStatus
        //vm.mypost.create_date = new Date();
        console.log("vm.mypost" + JSON.stringify(vm.mypost));
        PostFeeds
          .insertFeeds(vm.mypost)
          .then(function(results){
            console.log("result " + JSON.stringify(result));
          })
          .catch(function(err){
            console.log("error " + JSON.stringify(err));
          });
      };   

      // simulate posts from friends by randomly tweaking the messages timestamp
      vm.refreshFeeds = function() {
        for (i in vm.friends) {
          vm.friends[i].messages[0].timestamp = randTstamp();
        }
        // sort message timestamp
        vm.reindexBytime = friends.sort(function(a, b){return(b.messages[0].timestamp - a.messages[0].timestamp)});
        for (i in vm.reindexBytime) {
          console.log("i = %s, id = %d, timestamp = %s", i, vm.reindexBytime[i].id, vm.reindexBytime[i].messages[0].timestamp);
        }
      };
      // this is to initialize the feeds
      vm.refreshFeeds()   

    }

})();