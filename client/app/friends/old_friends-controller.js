(function () {
  angular
    .module("friendApp")
    .controller("FriendCtrl", ["$stateParams", FriendCtrl]);

    function FriendCtrl($stateParams){
        var vm = this;

      // models to add friends
      vm.friends = friends;
      vm.friendName = "";
      vm.contactNo = "";
      vm.email = "";
      vm.gender = "";
      vm.imageUrl = "/../assets/images/img2.gif";
      vm.group = "";
      vm.currentGroups = ["Acquaintance","Close","My Precious!","Family"];
      vm.messages = [
        {
          "timestamp": Date.now(),
          "tome": "hey we're now friends! awesomeness!!!",
          "fromme": "hello"
        }
      ];

      // hide/show add friends div
      vm.addFriends = function(){
        vm.formAddFriends = true;
        vm.btnAddFriends = false;
      }

      // function to add friends array thru push
      vm.updateFriends = function(){

        vm.formAddFriends = false;
        vm.btnAddFriends = true;
        // var friendCount = vm.friends.length
        var newFriend = {
          "id":friendCount = vm.friends.length,
          "friendName":vm.friendName,
          "contactNo":vm.contactNo,
          "email":vm.email,
          "gender":vm.gender,
          "imageUrl":vm.imageUrl,
          "group":vm.group,
          "messages":vm.messages
        }
        vm.friends.push(newFriend)
        console.log(vm.friends);
        
      }

    }

})();