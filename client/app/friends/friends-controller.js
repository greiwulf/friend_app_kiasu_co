(function () {
  angular
    .module("friendApp")
    .controller("FriendCtrl", FriendCtrl);
    
    FriendCtrl.$inject = ['$state', 'UserService'];
    function FriendCtrl($state, UserService){
        var vm = this;
        
      // models to add friends
      vm.friends = {
        name: "",
        contactNo: "",
        email: "",
        gender: "",
        group: "",
        photo: ""
      }
      vm.status = {
        message: "",
        code: ""
      }
      vm.currentGroups = [{"group":"Acquaintance"},{"group":"Close"},{"group":"Colleagues"},{"group":"Family"}];
      console.log(vm.currentGroups);
/*
      vm.friendName = "";
      vm.contactNo = "";
      vm.email = "";
      vm.gender = "";
      vm.imageUrl = "/../assets/images/img2.gif";
      vm.group = "";
      vm.currentGroups = ["Acquaintance","Close","My Precious!","Family"];
      vm.messages = [
        {
          "timestamp": Date.now(),
          "tome": "hey we're now friends! awesomeness!!!",
          "fromme": "hello"
        }
      ];
      */
      //expose the functions
      vm.addFriend = addFriend;
      // hide/show add friends div
     function addFriend() {
       console.log('adding record');
       UserService
        .insertFriend(vm.friends)
        .then(function(result) {
            console.log("result" + JSON.stringify(result));
            $state.go("Thanks")
        .catch(function() {
          console.log("error" + JSON.stringify(err));
          vm.status.message = err.data.name;
          vm.status.code = err.data.parenterrno;
        })
        })
     }
      
      // function to add friends array thru push
      vm.updateFriends = function(){
        vm.formAddFriends = false;
        vm.btnAddFriends = true;
        // var friendCount = vm.friends.length
        var newFriend = {
          "id":friendCount = vm.friends.length,
          "friendName":vm.friendName,
          "contactNo":vm.contactNo,
          "email":vm.email,
          "gender":vm.gender,
          "imageUrl":vm.imageUrl,
          "group":vm.group,
          "messages":vm.messages
        }
        vm.friends.push(newFriend)
        console.log(vm.friends);
        
      }
    }
})();