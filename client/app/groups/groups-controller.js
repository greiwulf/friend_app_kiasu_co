(function(){
    angular
        .module("friendApp")
        .controller("GroupsCtrl", GroupsCtrl);

    GroupsCtrl.$inject = ["$state","UserService"];

    function GroupsCtrl(state, UserService){
        var vm = this;
        //vm.searchString = state.params.userId;
        vm.searchString = 1;
        vm.friends = {};
        //vm.updateUser = updateUser;

        initDetails();
        function initDetails(){
            UserService
                .retrieveFriends(vm.searchString)
                .then(function(result){
                    vm.friends = result.data;
                    console.log(vm.friends);  
                }).catch(function(error){
                    console.log(JSON.stringify(error));
                })
        }

        // function updateUser(){
        //     console.log("Updating user..");
        //     UserService
        //         .updateUser(vm.searchString, vm.user.email, vm.user.name, vm.user.gender, vm.user.dateOfBirth, vm.user.address, vm.user.country, vm.user.contactNumber)
        //         .then(function(response){
        //             console.log(JSON.stringify(response.data));
        //             vm.update = false;
        //         }).catch(function(err){
        //             console.log(err);
        //         });
        // }
    }

    

})();