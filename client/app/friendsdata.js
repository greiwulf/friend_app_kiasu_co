// TODO: only temporary, delete this file when database is ready

var friends = [ {
    "id" : 0,
   "friendName" : "Batman",
    "contactNo" : "+01 12345678",
    "email" : "Batman@justiceleague.com",
    "gender" :"male",
    "imageUrl":"/../assets/images/img3.gif",
    "group": "Close",
    "saved" : 1,
    "messages" : [
      {
        "timestamp": new Date(),
        "tome": "If they have an Ice Cream Truck for kids, why don’t that have a Beer Truck for adults?",
        "fromme": "hello"
      }
    ]
  },
  {
    "id": 1,
    "friendName" : "WonderWoman",
    "contactNo" : "+01 23456789",
    "email" : "WonderWoman@justiceleague.com",
    "gender" :"female",
    "imageUrl":"/../assets/images/img5.gif",
    "group": "My Precious!",
    "saved" : 1,
    "messages" : [
      {
        "timestamp": new Date(),
        "tome": "hey, sup?",
        "fromme": "hello"
      }
    ]
  },
  {
    "id": 2,
    "friendName" : "SuperMan",
    "contactNo" : "+01 87654321",
    "email" : "SuperMan@justiceleague.com",
    "gender" :"male",
    "imageUrl":"/../assets/images/img9.png",
    "group": "Family",
    "saved" : 1,
    "messages" : [
      {
        "timestamp": new Date(),
        "tome": "I was in a cafe and needed to fart. The music was so loud so I timed them to the beat of the music. Then I realised I was listening to my ipod.",
        "fromme": "hello"
      }
    ]
  },
  {
    "id": 3,
    "friendName" : "Wolverine",
    "contactNo" : "+01 88885555",
    "email" : "Wolverine@xmen.com",
    "gender" :"male",
    "imageUrl":"/../assets/images/img1.gif",
    "group": "Family",
    "saved" : 1,
    "messages" : [
      {
        "timestamp": new Date(),
        "tome": "you're so awesome man!",
        "fromme": "hello"
      }
    ]
  },
  {
    "id": 4,
    "friendName" : "IronMan",
    "contactNo" : "+01 098765432",
    "email" : "IronMan@avengers.com",
    "gender" :"male",
    "imageUrl":"/../assets/images/img11.jpg",
    "group": "Acquaintance",
    "saved" : 1,
    "messages" : [
      {
        "timestamp": new Date(),
        "tome": "Congratulations!!! You are the 1000000th person to view my status. To see your prize please click Alt+F4.",
        "fromme": "hello"
      }
    ]
  },
  {
    "id": 5,
    "friendName" : "Hulk",
    "contactNo" : "+01 0000000000",
    "email" : "Hulk@avengers.com",
    "gender" :"male",
    "imageUrl":"/../assets/images/img4.gif",
    "group": "Acquaintance",
    "saved" : 1,
    "messages" : [
      {
        "timestamp": new Date(),
        "tome": "A lot of people say that social media is making us all dumber, but I not think that.",
        "fromme": "hello"
      }
    ]
  }    
];
  
