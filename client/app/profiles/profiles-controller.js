(function(){
    angular
        .module("friendApp")
        .controller("ProfileCtrl", ProfileCtrl);

    ProfileCtrl.$inject = ["$state","UserService"];

    function ProfileCtrl(state, UserService){
        var vm = this;
        vm.searchString = state.params.userId;
        //vm.searchString = 'me';

        vm.updateUser = updateUser;
        vm.initDetails = initDetails;
        vm.user = {
            email: "",
            password: "",
            name: "",
            gender: "F",
            dateOfBirth: "",
            address: "",
            country: "",
            contactNumber: ""
        };

        initDetails();
        function initDetails(){
            UserService
                .retrieveUser(vm.searchString)
                .then(function(result){
                    vm.user.email = result.data.email;
                    vm.user.name = result.data.first_name;
                    vm.user.gender = result.data.gender;
                    vm.user.dateOfBirth = new Date(result.data.birth_date);
                    vm.user.address = result.data.address;
                    vm.user.country = result.data.country;
                    vm.user.contactNumber = result.data.contact_no;
                      
                }).catch(function(error){
                    console.log(JSON.stringify(error));
                })
        }

        function updateUser(){
            console.log("Updating user..");
            UserService
                .updateUser(vm.searchString, vm.user.email, vm.user.name, vm.user.gender, vm.user.dateOfBirth, vm.user.address, vm.user.country, vm.user.contactNumber)
                .then(function(response){
                    console.log(JSON.stringify(response.data));
                    vm.update = false;
                }).catch(function(err){
                    console.log(err);
                });
        }
    }

    

})();