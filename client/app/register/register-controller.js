(function () {
    angular
        .module("friendApp")          
        .controller("RegCtrl", RegCtrl);  

    RegCtrl.$inject = ['$filter', '$state', 'UserService'];

    function RegCtrl($filter, $state, UserService) {

        var vm = this;
        vm.validEmail = true;

        vm.user = {
            email: "",
            password: "",
            name: "",
            gender: "F",
            dateOfBirth: "",
            address: "",
            country: "",
            contactNumber: ""
        };

        vm.status = {
            message: ""
            , code: ""
        };

        vm.register = register;
        vm.emailCheck = checkEmail;

        function register() {

            vm.user.join_date = new Date();
            // vm.department.to_date = new Date('9999-01-01');
            console.log("vm.user" + JSON.stringify(vm.user));

            UserService
                .insertUser(vm.user)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $state.go("Profiles", { userId: result.data.id });
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });
        }

        function checkEmail() {
            UserService
                .checkEmail(vm.user.email)
                .then(function(result){
                    if (result.data == null ) {
                        vm.validEmail = true;
                    }else {
                        vm.validEmail = false;
                    }
                      
                }).catch(function(error){
                    console.log(JSON.stringify(error));
                })

        }
    }

})();