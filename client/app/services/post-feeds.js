// IIFE
(function(){
  angular
  .module("friendApp")
  .service("PostFeeds", PostFeeds);

  // DI
  PostFeeds.$inject = ['$http'];

  // service function
  function PostFeeds($http) {
    var service = this;

    //expose service
    service.insertFeeds = insertFeeds;

    // fetch function
    function insertFeeds(feeds) {
      return $http({
        method: 'POST'
        , url: 'api/feeds'
        , data: feeds
      });
    }

  }

})();