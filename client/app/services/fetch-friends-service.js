// IIFE
(function(){
  angular
  .module("friendApp")
  .service("FetchFriends", FetchFriends);

  // DI
  FetchFriends.$inject = ['$http'];

  // service function
  function FetchFriends($http) {
    var service = this;

    //expose service
    service.fetchFriends = fetchFriends;

    // fetch function
    function fetchFriends() {
      return $http({
        method: 'GET'
        , url: 'api/friends'
      });
    }

  }

})();