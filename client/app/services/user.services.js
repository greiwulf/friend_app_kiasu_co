(function () {
    angular
        .module("friendApp")
        .service("UserService", UserService);

    UserService.$inject = ['$http'];

    function UserService($http) {

        var service = this;

        // service.deleteDept = deleteDept;
        service.insertUser = insertUser;
        service.checkEmail = checkEmail;
        service.retrieveUser = retrieveUser;
        // service.retrieveDeptByID = retrieveDeptByID;
        // service.retrieveDeptManager = retrieveDeptManager;
        service.updateUser = updateUser;
        service.retrieveFriends = retrieveFriends;
        service.insertFriend = insertFriend;

        // function deleteDept(dept_no, emp_no) {
        //     return $http({
        //         method: 'DELETE'
        //         , url: 'api/departments/' + dept_no + "/managers/" + emp_no
        //     });

        // }

        function insertUser(user) {
            return $http({
                method: 'POST'
                , url: 'api/user'
                , data: {user: user}
            });
        }
        function insertFriend(friend) {
            return $http({
                method: 'POST'
                , url: 'api/add_friend'
                , data: {friend: friend}
            });
        }

        // function retrieveDept() {
        //     return $http({
        //         method: 'GET'
        //         , url: 'api/static/departments'
        //     });
        // }

        function retrieveUser(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/user'
                , params: {
                    'searchString': searchString
                }
            });
        }

        function checkEmail(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/email'
                , params: {
                    'searchString': searchString
                }
            });
        }

        function retrieveFriends(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/friends'
                , params: {
                    'searchString': searchString
                }
            });
        }
        // function retrieveDeptByID(dept_no) {
        //     return $http({
        //         method: 'GET'
        //         , url: "api/departments/" + dept_no
        //     });
        // }

        // function retrieveDeptManager(searchString) {
        //     return $http({
        //         method: 'GET'
        //         , url: 'api/departments/managers'
        //         , params: {
        //             'searchString': searchString
        //         }
        //     });
        // }

        function updateUser(id, email, name, gender, birth_date, address, country, contact_no) {
            return $http({
                method: 'PUT'
                , url: 'api/user/' + id
                , data: {
                    id: id,
                    email: email,
                    first_name: name,
                    gender: gender,
                    birth_date: birth_date,
                    address: address,
                    country: country,
                    contact_no: contact_no
                }
            });
        }
    }
})();