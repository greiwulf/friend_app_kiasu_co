(function(){

  angular
    .module("friendApp")
    .config(uirouterAppConfig);
  uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

  function uirouterAppConfig($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('Home', {
        url : '/Home',
        templateUrl: 'app/home/home.html',
        controller : 'HomeCtrl',
        controllerAs : 'ctrl'
      })
      .state('Friends', {
        url : '/Friends',
        templateUrl: 'app/friends/friends.html',
        controller : 'FriendCtrl',
        controllerAs : 'ctrl'
      })
      .state('Profiles', {
        url : '/Profiles/:userId',
        templateUrl: 'app/profiles/profiles.html',
        controller : 'ProfileCtrl',
        controllerAs : 'ctrl'
      })
      .state('Register', {
        url : '/Register',
        templateUrl: 'app/register/register.html',
        controller : 'RegCtrl',
        controllerAs : 'ctrl'
      })
      .state('Groups', {
        url : '/Groups',
        templateUrl: 'app/groups/groups.html',
        controller : 'GroupsCtrl',
        controllerAs : 'ctrl'
      })
      .state('Thanks', {
        url : '/Thanks',
        templateUrl: 'app/friends/thanks.html',
      })
    $urlRouterProvider.otherwise("/Home");
  }

})();