-- Friends App database by Kiasu Co

DROP DATABASE IF EXISTS friends;
CREATE DATABASE IF NOT EXISTS friends;
USE friends;

SELECT 'CREATING DATABASE STRUCTURE' as 'INFO';

DROP TABLE IF EXISTS users,
                     groups,
                     groupings, 
                     photos,
                     feeds,
                     friends;

CREATE TABLE users (
  username      VARCHAR(16)     NOT NULL,
  password      VARCHAR(16)     NOT NULL,
  telno         VARCHAR(16)     NOT NULL,
  first_name    VARCHAR(16)     NOT NULL,
  last_name     VARCHAR(16)     NOT NULL,
  email         VARCHAR(32)     NOT NULL,
  gender        ENUM ('M','F')  NOT NULL, 
  birth_date    VARCHAR(10)     NOT NULL,
  address       VARCHAR(255),
  country       VARCHAR(255),
  join_date     TIMESTAMP       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (username)
);

CREATE TABLE groups (
  group_name    VARCHAR(16)     NOT NULL,
  group_desc    VARCHAR(32)     NOT NULL,
  create_date   TIMESTAMP       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (group_name)
);

CREATE TABLE groupings (
  username      VARCHAR(16)     NOT NULL,
  group_name    VARCHAR(16)     NOT NULL,
  create_date   TIMESTAMP       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY         (username),
  KEY         (group_name),
  FOREIGN KEY (username)  REFERENCES users (username)    ON DELETE CASCADE,
  FOREIGN KEY (group_name) REFERENCES groups (group_name) ON DELETE CASCADE,
  PRIMARY KEY (username, group_name)
); 

CREATE TABLE friends (
  name          VARCHAR(16)     NOT NULL,
  contactno    VARCHAR(16),
  email         VARCHAR(32)     NOT NULL,
  gender        ENUM ('M','F')  NOT NULL,
  group_name         VARCHAR(16),
  photo         VARCHAR(512)    NOT NULL,
  create_date   TIMESTAMP       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (name) REFERENCES groupings (username) ON DELETE CASCADE,
  PRIMARY KEY (name)
);

CREATE TABLE photos (
  id int NOT NULL AUTO_INCREMENT,
  username      VARCHAR(16)     NOT NULL,
  path          VARCHAR(512)    NOT NULL,
  img          VARCHAR(512)    NOT NULL,
  create_date   TIMESTAMP       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY         (username),
  FOREIGN KEY (username)  REFERENCES users (username)    ON DELETE CASCADE,
  PRIMARY KEY (id)
);

CREATE TABLE feeds (
  id int NOT NULL AUTO_INCREMENT,
  from_username     VARCHAR(16)     NOT NULL,
  to_username       VARCHAR(16)     NOT NULL,
  msg               VARCHAR(512)    NOT NULL,
  create_date       TIMESTAMP       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY         (from_username),
  FOREIGN KEY (from_username)  REFERENCES users (username)    ON DELETE CASCADE,
  PRIMARY KEY (id)
);

-- load default data
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('batman','batman','+01 12345678','batman','dc','batman@justice.com','M','1980-01-01','VSXTiXExKBLKriHSw9','Cape Verde');
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('wonderwoman','wonderwoman','+01 23456789','wonderwoman','dc','wonderwoman@justice.com','F','1981-02-02','A3MVQC','Guernsey');
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('captamerica','captain','+01 87654321','america','avengers','captamerica@avengers.com','M','1983-03-03','H59NW','Yemen');
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('blackwidow','blackwidow','+01 88885555','blackwidow','avengers','blackwidow@avengers.com','F','1984-04-04','4gKXHBP6pZPDwAtF4Y','Bahamas');
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('profx','profx','+01 098765432','profx','xmen','profx@xmen.com','M','1974-05-06','KTwwW','Colombia');
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('psylocke','psylocke','+01 0000000000','psylocke','xmen','psylocke@xmen.com','F','1975-06-07','Tjb1Rb','New Caledonia');
INSERT INTO `users` (`username`,`password`,`telno`,`first_name`,`last_name`,`email`,`gender`,`birth_date`,`address`,`country`) VALUES ('me','me','myself','88888888','isawesome','me@awesome.com','M','1975-07-08','FPBfaSba','Puerto Rico');

INSERT INTO `groups` (`group_name`,`group_desc`) VALUES ('family','family');
INSERT INTO `groups` (`group_name`,`group_desc`) VALUES ('relatives','relatives');
INSERT INTO `groups` (`group_name`,`group_desc`) VALUES ('close','close');
INSERT INTO `groups` (`group_name`,`group_desc`) VALUES ('acquaintance','acquaintance');
INSERT INTO `groups` (`group_name`,`group_desc`) VALUES ('my precious','love');

INSERT INTO `groupings` (`username`,`group_name`) VALUES ('batman','close');
INSERT INTO `groupings` (`username`,`group_name`) VALUES ('wonderwoman','acquaintance');
INSERT INTO `groupings` (`username`,`group_name`) VALUES ('profx','relatives');
INSERT INTO `groupings` (`username`,`group_name`) VALUES ('captamerica','family');
INSERT INTO `groupings` (`username`,`group_name`) VALUES ('blackwidow','my precious');
INSERT INTO `groupings` (`username`,`group_name`) VALUES ('psylocke','close');

INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('me','all',"future's so bright, gotta wear shades!");
INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('batman','all',"If they have an Ice Cream Truck for kids, why don’t that have a Beer Truck for adults?");
INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('wonderwoman','me',"current relationship status: sleeping diagonally across my queen size bed");
INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('captamerica','all',"I was in a cafe and needed to fart. The music was so loud so I timed them to the beat of the music. Then I realised I was listening to my ipod.");
INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('profx','all',"Congratulations!!! You are the 1000000th person to view my status. To see your prize please click Alt+F4.");
INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('psylocke','all',"A lot of people say that social media is making us all dumber, but I not think that.");
INSERT INTO `feeds` (`from_username`,`to_username`, `msg`) VALUES ('blackwidow','all',"What happens in Vegas stays in Vegas; what happens on Twitter stay on Google forever!");

INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('me','/../assets/images/img6.gif','/../assets/images/img6.gif');
INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('batman','/../assets/images/img3.gif','/../assets/images/img3.gif');
INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('wonderwoman','/../assets/images/img5.gif','/../assets/images/img5.gif');
INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('captamerica','/../assets/images/img9.png','/../assets/images/img9.png');
INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('profx','/../assets/images/img1.gif','/../assets/images/img1.gif');
INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('psylocke','/../assets/images/img2.gif','/../assets/images/img2.gif');
INSERT INTO `photos` (`username`,`path`, `img`) VALUES ('blackwidow','/../assets/images/img4.gif','/../assets/images/img4.gif');

INSERT INTO `friends` (`name`,`contactno`, `email`, `gender`, `photo`) VALUES ('batman','135.243.221.239','vozi@guvugioj.ht','M','http://am.hu/kiwu');
INSERT INTO `friends` (`name`,`contactno`, `email`, `gender`, `photo`) VALUES ('wonderwoman','228.195.46.214','bedtokib@pa.ad','F','http://fofo.hn/junebe');
INSERT INTO `friends` (`name`,`contactno`, `email`, `gender`, `photo`) VALUES ('profx','137.33.69.183','ramhu@hog.mg','M','http://nede.by/nuj');
INSERT INTO `friends` (`name`,`contactno`, `email`, `gender`, `photo`) VALUES ('captamerica','98.2.124.15','imi@falusef.na','M','http://jimcib.il/pavagbuf');
INSERT INTO `friends` (`name`,`contactno`, `email`, `gender`, `photo`) VALUES ('blackwidow','207.217.82.119','enefiig@bu.np','F','http://ododocid.mo/mercaduhi');
INSERT INTO `friends` (`name`,`contactno`, `email`, `gender`, `photo`) VALUES ('psylocke','151.43.213.201','cumbunpod@noh.am','F','http://ujonev.al/var');
