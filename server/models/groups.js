module.exports = function(conn, Sequelize){
 var Groups = conn.define('groups', 
    {
      group_name:{
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      group_desc:{
        type: Sequelize.STRING,
        allowNull: false
      },
      create_date: {
        type: Sequelize.DATE
        // allowNull: false
      }
    },{
        tableName: "groups",
        timestamps: false
    });
 return Groups;
};