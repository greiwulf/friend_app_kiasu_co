module.exports = function(conn, Sequelize) { 
    var Groups = conn.define("groups", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true
        },        
        name: {
            type: Sequelize.STRING,
            allowNull: false,              
        },
        desc: {
            type: Sequelize.TEXT,
            allowNull:false
        },
        type: {
            type: Sequelize.STRING,
            //type: Sequelize.ENUM("personal", "public", "private"),
            allowNull:false
        },
        category: {
            type: Sequelize.STRING,
            allowNull:false
        },        
        creator_id: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        }
    },{
        freezeTableName: true,
        timestamps: false
    });
    return Groups;
};