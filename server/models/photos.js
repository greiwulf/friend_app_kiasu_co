module.exports = function(conn, Sequelize){
var Photos = conn.define('photos', {
  id:{
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false
  },
  username:{
    type: Sequelize.STRING,
    allowNull: false,
    references: {
      model: 'users',
      key: 'username'
    }
  },
  path:{
    type: Sequelize.STRING,
    allowNull: false
  },
  img:{
    type: Sequelize.STRING,
    allowNull: false
  },
    create_date: {
      type: Sequelize.DATE
      // allowNull: false
    }
},{
     tableName: "photos",
     timestamps: false
});
 return Photos;
};