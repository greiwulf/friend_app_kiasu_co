module.exports = function(conn, Sequelize) { 
    var Feed = conn.define("feed", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true
        }, 
        user_id: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        content: {
            type: Sequelize.TEXT,
            allowNull:false
        },
        time_created: {
            type: Sequelize.DATE,
            allowNull:false
        }

    },{
        freezeTableName: true,
        timestamps: false
    });
    return Feed;
};