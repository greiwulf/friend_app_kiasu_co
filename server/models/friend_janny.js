module.exports = function(conn, Sequelize) { 
    var Friends = conn.define("friends", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull:false
        },
        contactNo: {
            type: Sequelize.STRING,
            allowNull:true
        }, 
        email: {
            type: Sequelize.STRING,
            allowNull:false,
            unique: true
        },
        gender: {
            type: Sequelize.ENUM("M", "F"),
            allowNull:false
        },
        group: {
            type: Sequelize.STRING,
            allowNull:true
        },
        photo: {
            type: Sequelize.STRING,
        }
    },{
        freezeTableName: true,
        timestamps: false
    });
    return Friends;
};