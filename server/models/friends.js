module.exports = function(conn, Sequelize) { 
    var Friends = conn.define("friend", {
        name: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false
        }, 
        contactno: {
            type: Sequelize.STRING,
            allowNull: true
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
      gender: {
        type: Sequelize.ENUM('M','F'),
        allowNull: false
      },
        group: {
            type: Sequelize.STRING,
            allowNull: false
        },
        photo: {
            type: Sequelize.STRING,
            allowNull: false
        },
        create_date: {
            type: Sequelize.DATE,
            allowNull:false
        }
    },{
        freezeTableName: true,
        timestamps: false
    });
    return Friends;
};