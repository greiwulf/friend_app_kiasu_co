module.exports = function(conn, Sequelize){
 var Feeds = conn.define('feeds', {
  id:{
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  from_username:{
    type: Sequelize.STRING,
    allowNull: false,
    references: {
      model: 'users',
      key: 'username'
    }
  },
  to_username:{
    type: Sequelize.STRING,
    allowNull: false
  },
  msg:{
    type: Sequelize.STRING,
    allowNull: false
  },
    create_date: {
      type: Sequelize.DATE
      //allowNull: false
    }
 },{
     tableName: "feeds",
     timestamps: false
 });
 return Feeds;
};