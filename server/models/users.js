module.exports = function(conn, Sequelize){
 var Users = conn.define('users', 
    {
      username:{
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      password:{
        type: Sequelize.STRING,
        allowNull: false        
      },
      telno:{
        type: Sequelize.STRING,
        allowNull: false        
      },
      first_name:{
        type: Sequelize.STRING,
        allowNull: false        
      },
      last_name:{
        type: Sequelize.STRING,
        allowNull: false        
      },
      email:{
        type: Sequelize.STRING,
        allowNull: false        
      },
      gender: {
        type: Sequelize.ENUM('M','F'),
        allowNull: false
      },
      birth_date:{
        type: Sequelize.STRING,
        allowNull: false        
      },
      address:{
        type: Sequelize.STRING,
        allowNull: true        
      },
      country:{
        type: Sequelize.STRING,
        allowNull: true        
      },
      join_date: {
        type: Sequelize.DATE
        // allowNull: false
      }
    },{
      tableName: "users",
      timestamps: false
    });
 return Users;
};