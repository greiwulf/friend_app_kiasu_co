module.exports = function(conn, Sequelize) { 
    var Photo = conn.define("photo", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true
        }, 
        name: {
            type: Sequelize.STRING,
        },
        desc: {
            type: Sequelize.TEXT,
        },
        file: {
            type: Sequelize.BLOB,
            allowNull:false
        },
        category: {
            type: Sequelize.STRING,
            allowNull:false
        },
        user_id: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        time_created: {
            type: Sequelize.DATE,
            allowNull:false
        }

    },{
        freezeTableName: true,
        timestamps: false
    });
    return Photo;
};