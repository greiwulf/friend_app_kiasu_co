module.exports = function(conn, Sequelize) { 
    var GroupMembers = conn.define("group_members", {
        grp_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            references: {
                model: 'group',
                key: 'id'
            }
        },        
        member_id: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        status: {
            type: Sequelize.STRING,
            allowNull:false
        }
    },{
        freezeTableName: true,
        timestamps: false
    });
    return GroupMembers;
};