module.exports = function(conn, Sequelize) { 
    var User = conn.define("user", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true
        },
        email: {
            type: Sequelize.STRING,
            allowNull:false,
            unique: true
            // , validate: {
            //     isEmail: true
            // }
        },
        password: {
            type: Sequelize.STRING,
            allowNull:false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull:false
        },
        last_name: {
            type: Sequelize.STRING,
            allowNull:false
        },
        gender: {
            type: Sequelize.ENUM("M", "F"),
            allowNull:false
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull:false
        },
        address: {
            type: Sequelize.STRING,
            allowNull:true
        },
        country: {
            type: Sequelize.STRING,
            allowNull:true
        },
        contact_no: {
            type: Sequelize.STRING,
            allowNull:true
        },          
        join_date: {
            type: Sequelize.DATE,
            allowNull:false
        }
    },{
        freezeTableName: true,
        timestamps: false
    });
    return User;
};