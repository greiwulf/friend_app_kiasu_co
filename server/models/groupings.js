module.exports = function(conn, Sequelize){
var Groupings = conn.define('groupings', 
  {
    username:{
      type: Sequelize.STRING,
      primaryKey: true,
      allowNull: false,
      references: {
        model: 'users',
        key: 'username'
      }      
    },
    group_name:{
      type: Sequelize.STRING,
      primaryKey: true,
      allowNull: false,
      references: {
        model: 'groups',
        key: 'group_name'
      }      
    },
    create_date: {
      type: Sequelize.DATE
      // allowNull: false
    }
  },{
      tableName: "groupings",
      timestamps: false
  });
 return Groupings;
};