// load libraries
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var Sequelize = require('sequelize');

// declare mysql credentials
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "re1nald0";  

// declare port
const NODE_PORT = process.env.NODE_PORT || 8080;

// declare folders
const CLIENT_FOLDER = path.join(__dirname , '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, 'assets/messages');
const HOME_FOLDER = path.join(CLIENT_FOLDER, 'app/home');
const FRIENDS_FOLDER = path.join(CLIENT_FOLDER, 'app/friends');
const PROFILES_FOLDER = path.join(CLIENT_FOLDER, 'app/profiles');
const REGISTER_FOLDER = path.join(CLIENT_FOLDER, 'app/register');

//Sequelize connect
var conn = new Sequelize(
    'friends',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
      host: 'localhost',
      logging: console.log,
      dialect: 'mysql',
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      }
    }
  );  

// import the database models into the app.js
var Users = require('./models/users')(conn, Sequelize);
var Photos = require('./models/photos')(conn, Sequelize);
var Groups = require('./models/groups')(conn, Sequelize);
var Feeds = require('./models/feeds')(conn, Sequelize);
var Groupings = require('./models/groupings')(conn, Sequelize);
var Friends = require('./models/friends')(conn, Sequelize);

//create an instance of express application
var app = express();
  
// Map the data association
Users.hasMany(Groupings, {foreignKey: 'username'});
Groupings.hasMany(Friends, {foreignKey: 'username'});
Users.hasMany(Photos, {foreignKey: 'username'});
Users.hasMany(Feeds, {foreignKey: 'from_username'});
  
// declare routes
app.use(express.static(CLIENT_FOLDER));
app.use(express.static(MSG_FOLDER));
app.use(express.static(HOME_FOLDER));
app.use(express.static(FRIENDS_FOLDER));
app.use(express.static(PROFILES_FOLDER));
app.use(express.static(REGISTER_FOLDER));

// setup of the configuration of express
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended : false}));

// api calls
// fetch friends
app.get('/api/friends', function(req, res){
  console.log(req.body);
  Users
    .findAll({
      include: [
        {model: Groupings},
        {model: Photos},
        {model: Feeds}
      ]
    })
    .then(function(friends){
      res
        .status(200)
        .json(friends);
    })
    .catch(function(err){
      res
        .status(500)
        .json(err);
    })
});

// post feeds
app.post('/api/feeds', function(req, res){
  console.log("server call: " + JSON.stringify(req.body));
  Feeds
  .create({
    from_username: req.body.from_username,
    to_username: req.body.to_username,
    msg: req.body.msg
  })
  .then(function(results){
    console.log(results.get({plain: true}));
    res
    .status(200)
    .json(results);
  })
  .catch(function(err){
    console.log("error: " + err);
    res
    .status(500)
    .json(err);
  })
});

// require("./app_lewis")(app);
// add user
app.post("/api/user", function(req, res){        
  console.log("Inserting record");
  console.log(req.body);
  Users
    .create({
        email: req.body.user.email,
        password: req.body.user.password,
        first_name: req.body.user.name,
        last_name: req.body.user.name,
        gender: req.body.user.gender,
        birth_date: req.body.user.dateOfBirth,
        address: req.body.user.address,
        country: req.body.user.country,
        telno: req.body.user.contactNumber,
        join_date: req.body.user.join_date
    }).then(function(record){
        res.status(200).json(record);
    }).catch(function(err){
        res.status(500).json(err);
    });

});

// add friend
app.post("/api/add_friend", function(req, res){        
console.log("Inserting friend");
console.log(req.body);
Friends
    .create({
        name: req.body.friend.name,
        contactno: req.body.friend.contactNo,
        email: req.body.friend.email,
        gender: req.body.friend.gender,
        group: req.body.friend.group,
        photo: req.body.friend.photo
    }).then(function(record){
        res.status(200).json(record);
    }).catch(function(err){
        res.status(500).json(err);
    });

});

app.get("/api/user", function(req, res){
    console.log(req.query.searchString);
    Users
        .findOne({
            where: {
                username: req.query.searchString
                //id: req.query.searchString
            }
        }).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })
});

app.get("/api/email", function(req, res){
    console.log(req.query.searchString);
    Users
        .findOne({
            where: {
                email: req.query.searchString
            }
        }).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })
});

app.put("/api/user/:id", function(req,res){
var where = {};
where.id = req.params.id;
//where.dept_name = req.params.dept_name;
Users
    .update({email: req.body.email
            , first_name: req.body.first_name
            , gender: req.body.gender
            , birth_date: req.body.birth_date
            , address: req.body.address
            , country: req.body.country
            , contact_no: req.body.contact_no
    },
        {where: where}
    ).then(function(result) {
        res 
            .status(200)
            .json(result);
    }).catch(function(err){
        console.log(err);
        res
            .status(500)
            .json(err);
    })
});

app.get("/api/friends", function(req, res){
    console.log(req.query.searchString);
    Friends
        .findAll({
            where: {
                sender_id: req.query.searchString
            }
        }).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })
});

// error 404 route
app.use(function(req,res){
    res.status(400).sendFile(path.join(CLIENT_FOLDER, "/assets/messages/404.html"));
});

// error 500 route
app.use(function(err, req, res, next){
    console.log("An error had occured 500");
    res.status(500).sendFile(path.join(CLIENT_FOLDER, "/assets/messages/500.html"));
});

//start the server
app.listen(NODE_PORT, function() {
  console.log("Application started at %s on %d", new Date(), NODE_PORT);
});
