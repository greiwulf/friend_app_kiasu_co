
var Sequelize = require('sequelize');
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "mySQ1!sql"; 

var conn = new Sequelize(
    'friendApp',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
      host: 'localhost',
      logging: console.log,
      dialect: 'mysql',
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      }
    }
);  

// var Groups = require('./models/groups')(conn, Sequelize);
var User = require('./models/user')(conn, Sequelize);
var Friend = require('./models/friend')(conn, Sequelize);
var Friends = require('./models/friend_janny')(conn, Sequelize);
// var Feed = require('./models/feed')(conn, Sequelize);
// var Photo = require('./models/photo')(conn, Sequelize);
// var GroupMembers = require('./models/group_members')(conn, Sequelize);

//Friend.hasOne(User, {foreignKey: 'sender_id', constraints: false});

module.exports = function(app){

    app.post("/api/user", function(req, res){        
    console.log("Inserting record");
    console.log(req.body);
    User
        .create({
            email: req.body.user.email,
            password: req.body.user.password,
            first_name: req.body.user.name,
            last_name: req.body.user.name,
            gender: req.body.user.gender,
            birth_date: req.body.user.dateOfBirth,
            address: req.body.user.address,
            country: req.body.user.country,
            contact_no: req.body.user.contactNumber,
            join_date: req.body.user.join_date
        }).then(function(record){
            res.status(200).json(record);
        }).catch(function(err){
            res.status(500).json(err);
        });
    
    });
    
    app.post("/api/add_friend", function(req, res){        
    console.log("Inserting friend");
    console.log(req.body);
    Friends
        .create({
            name: req.body.friend.name,
            contactNo: req.body.friend.contactNo,
            email: req.body.friend.email,
            gender: req.body.friend.gender,
            group: req.body.friend.group,
            photo: req.body.friend.photo
        }).then(function(record){
            res.status(200).json(record);
        }).catch(function(err){
            res.status(500).json(err);
        });
    
    });

    app.get("/api/user", function(req, res){
        console.log(req.query.searchString);
        User
            .findOne({
                where: {
                    id: req.query.searchString
                }
            }).then(function(result) {
                res 
                    .status(200)
                    .json(result);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    });

    app.get("/api/email", function(req, res){
        console.log(req.query.searchString);
        User
            .findOne({
                where: {
                    email: req.query.searchString
                }
            }).then(function(result) {
                res 
                    .status(200)
                    .json(result);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    });
    
    app.put("/api/user/:id", function(req,res){
    var where = {};
    where.id = req.params.id;
    //where.dept_name = req.params.dept_name;
    User
        .update({email: req.body.email
                , first_name: req.body.first_name
                , gender: req.body.gender
                , birth_date: req.body.birth_date
                , address: req.body.address
                , country: req.body.country
                , contact_no: req.body.contact_no
        },
            {where: where}
        ).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            console.log(err);
            res
                .status(500)
                .json(err);
        })
    });

    app.get("/api/friends", function(req, res){
        console.log(req.query.searchString);
        Friend
            .findAll({
                where: {
                    sender_id: req.query.searchString
                }
            }).then(function(result) {
                res 
                    .status(200)
                    .json(result);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    });
    // app.get("/update-profile", function(req, resp){
    //     var user = req.query.username;
    //     var idx = profDetails.findIndex(x => x.username==user);
    //     profDetails[idx].name = req.query.name;
    //     profDetails[idx].email = req.query.email;
    //     resp.status(201).end();
    // });

    // app.get("/update-friend", function(req, resp){
    //     var myUser = req.query.myUser;
    //     var friendUser = req.query.friendUser;
    //     var idx = profDetails.findIndex(x => x.username==myUser);
    //     var idx2 = profDetails.findIndex(x => x.username==friendUser);
    //     var sendStat = req.query.status1;
    //     var receiveStat = req.query.status2;

    //     if (profDetails[idx].friend == "") {
    //         profDetails[idx].friend.push({user:friendUser, status:sendStat});
    //     }else {
    //         var fIdx = profDetails[idx].friend.findIndex(x => x.user==friendUser);
    //         //console.log(fIdx);
    //         if (fIdx == -1) {
    //             profDetails[idx].friend.push({user:friendUser, status:sendStat});
    //         }else {
    //             profDetails[idx].friend[fIdx].status = sendStat;
    //         }
    //     }

    //     if (profDetails[idx2].friend == "") {
    //         profDetails[idx2].friend.push({user:myUser, status:receiveStat});
    //     }else {
    //         var fIdx2 = profDetails[idx2].friend.findIndex(x => x.user==myUser);
    //         //console.log(fIdx);
    //         if (fIdx2 == -1) {
    //             profDetails[idx2].friend.push({user:myUser, status:receiveStat});
    //         }else {
    //             profDetails[idx2].friend[fIdx2].status = receiveStat;
    //         }
    //     }

    //     console.log(profDetails[idx]);
    //     console.log(profDetails[idx2]);
    //     resp.status(201).end();
    // });

    //{force: true}
    conn.sync({force: true}).then(function () {
        console.log("Database in Sync Now")
    });
}